prototipo de proyecto Voxerox

Voxerox es un juego en 3D utilizando el estilo Voxel de diseño tanto para el protagonista y los personajes, como para su entorno.
La historia se basa en nuestro personaje, que vive en un mundo fantástico y peligroso en una pequeña casa en las colinas al norte de Voxerox.
El protagonista no puede salir de su casa debido a que hay bandidos en el acceso al bosque. Un día un fuerte temblor se siente y fuera de la casa aparece un hombre tirado en el piso. El hombre misterioso le entrega al protagonista un báculo con unas inscripciones talladas  con la promesa de volver a buscarlo y huye del lugar; a lo lejos unas sombras lo persiguen pasando del protagonista. El protagonista le muestra el báculo a su madre y esta lo manda al pueblo a averiguar sobre qué es y qué valor tiene. El protagonista se encuentra con los bandidos en medio del bosque y al tratar de defenderse con el báculo, las inscripciones talladas en él se encienden y este desprende una gran cantidad de energia al golpear.
(basado en las notas tomadas para el juego).
