﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seguimiento : MonoBehaviour
{
    public int distancia;
    public GameObject protagonista;
    // Start is called before the first frame update
    void Start()
    {
        if (distancia == 0)
        {
            distancia = 50;
        }
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(protagonista.transform.position.x - 4, protagonista.transform.position.y + distancia, protagonista.transform.position.z + distancia);
        //gameObject.transform.Translate(new Vector3(protagonista.transform.position.x - 4, protagonista.transform.position.y + 50, protagonista.transform.position.z + 50));
    }
}
