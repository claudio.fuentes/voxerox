﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Control : MonoBehaviour
{
    public int velMov;
    public GameObject suela;
    private OrientacionCara cara = OrientacionCara.sur;
    public GameObject colisionadorDeAtaque;
     BioPropiedades bprop;
    //Animator animacion;
    public GameObject personajeModelo;

    void Start()
    {
        bprop = new BioPropiedades(Especie.humano);
        //animacion = personajeModelo.GetComponent<Animator>();
    }

    void Update()
    {
        GirarCara();
        Mover();
        Atacar();
        
    }

    void FixedUpdate()
    {
            
        if (Input.GetKey(KeyCode.Space))
        {
            if (suela.GetComponent<ValidadorPiso>().Suelo())
            {
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 8f, 0);

            }
        }
    }

    void Atacar() {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            colisionadorDeAtaque.GetComponent<ValidacionAtaque>().Atacar(bprop.ataque);
            personajeModelo.GetComponent<Animar>().Atacar();
        }
    }
    void Mover() {
        if (Input.GetKey(KeyCode.W))
        {
            if (cara == OrientacionCara.norte)
            {
                gameObject.transform.Translate(0f, 0f, velMov * Time.deltaTime);
            }
            personajeModelo.GetComponent<Animar>().Caminar();
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (cara == OrientacionCara.sur)
            {
                gameObject.transform.Translate(0f, 0f, velMov * Time.deltaTime);
            }
            personajeModelo.GetComponent<Animar>().Caminar();
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (cara == OrientacionCara.oeste)
            {
                gameObject.transform.Translate(0f, 0f, velMov * Time.deltaTime);
            }
            personajeModelo.GetComponent<Animar>().Caminar();
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (cara == OrientacionCara.este)
            {
                gameObject.transform.Translate(0f, 0f, velMov * Time.deltaTime);
            }
            personajeModelo.GetComponent<Animar>().Caminar();
        }
    }
    void GirarCara() {
        int angulo = 0;
        if (Input.GetKeyDown(KeyCode.A))
        {
            switch (cara)
            {
                case OrientacionCara.norte:
                    angulo = 270;
                    break;
                case OrientacionCara.sur:
                    angulo = 90;
                    break;
                case OrientacionCara.este:
                    angulo = 180;
                    break;
                case OrientacionCara.oeste:
                    angulo = 0;
                    break;
                default:
                    break;
            }
            cara = OrientacionCara.oeste;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            switch (cara)
            {
                case OrientacionCara.norte:
                    angulo = 90;
                    break;
                case OrientacionCara.sur:
                    angulo = 270;
                    break;
                case OrientacionCara.este:
                    angulo = 0;
                    break;
                case OrientacionCara.oeste:
                    angulo = 180;
                    break;
                default:
                    break;
            }
            
            cara = OrientacionCara.este;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            switch (cara)
            {
                case OrientacionCara.norte:
                    angulo = 0;
                    break;
                case OrientacionCara.sur:
                    angulo = 180;
                    break;
                case OrientacionCara.este:
                    angulo = 270;
                    break;
                case OrientacionCara.oeste:
                    angulo = 90;
                    break;
                default:
                    break;
            }

            cara = OrientacionCara.norte;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            switch (cara)
            {
                case OrientacionCara.norte:
                    angulo = 180;
                    break;
                case OrientacionCara.sur:
                    angulo = 0;
                    break;
                case OrientacionCara.este:
                    angulo = 90;
                    break;
                case OrientacionCara.oeste:
                    angulo = 270;
                    break;
                default:
                    break;
            }

            cara = OrientacionCara.sur;
        }

        transform.Rotate(0, angulo, 0);
    }
}
