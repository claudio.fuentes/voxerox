﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dios : MonoBehaviour
{
    public GameObject protagonista;
    public Text lblVida;
    public Text lblEnx;
    public Text lblNivel;
    int vidaCompleta;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (vidaCompleta == 0)
        {
            vidaCompleta = (int)protagonista.GetComponent<BioPropiedades>().vida;
        }
        ActualizarPanelInformacion();
    }

    void ActualizarPanelInformacion() {
        int vida, enx, nivel;
        vida = (int)protagonista.GetComponent<BioPropiedades>().vida;
        //enx = (int)protagonista.GetComponent<BioPropiedades>().vida;
        nivel = (int)protagonista.GetComponent<BioPropiedades>().nivel;

        lblVida.text = "vida : " + vida + "/" + vidaCompleta;
        lblNivel.text = "nivel : " + nivel;
    }
}
