﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OrientacionCara {norte, sur, este, oeste}
public enum Especie { humano, vaca,ogro, buho, zorro,puma, araña, raton, quimera,mano}
public enum PropVida { vida, nivel, ataque, defensa, velocidadDesplazamiento, rangoAtaque, velocidadAtaque}
public enum PropPersona {  experiencia, ahcrox, dinero}
