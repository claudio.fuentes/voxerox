﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidacionAtaque : MonoBehaviour
{
    bool alerta = false;
    BioPropiedades bprop;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ValidarExistencia();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "mob")
        {
            alerta = true;
            bprop = other.GetComponent<BioPropiedades>();
            bprop.VistaVida(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "mob")
        {
            bprop.VistaVida(false);
            alerta = false;
            bprop = null;
        }
        
    }

    public void Atacar(float atk) {
        if (alerta)
        {
            bprop.RecibirAtaque(atk);
        }
    }
    public void ValidarExistencia() {
        if (alerta)
        {
            if (bprop == null)
            {
                alerta = false;
            }
        }
    }
}
