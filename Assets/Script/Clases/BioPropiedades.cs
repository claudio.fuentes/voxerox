﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BioPropiedades : MonoBehaviour
{
    public Especie especie;
    public float vida;
    public int nivel;
    public float ataque;
    public float defensa;
    public float velocidadDesplazamiento;
    public float rangoAtaque;
    public bool customizado = false;
    public GameObject barraVida;

    int contadorAtaque = 0;
    float vidaTotal;

    public BioPropiedades(Especie especie, float vida, int nivel , float ataque , float defensa , float velocidadDesplazamiento , float rangoAtaque )
    {
        this.especie = especie;
        this.vida = vida;
        this.nivel = nivel;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidadDesplazamiento = velocidadDesplazamiento;
        this.rangoAtaque = rangoAtaque;
    }
    public BioPropiedades(Especie especie)
    {
        this.especie = especie;
        AjustarValoresPorDefecto();
    }

    public void AjustarValoresPorDefecto() {
        if (!customizado)
        {
            vida = NormativaBioPropiedad.Defecto(PropVida.vida, especie);
            nivel = (int)NormativaBioPropiedad.Defecto(PropVida.nivel, especie);
            ataque = NormativaBioPropiedad.Defecto(PropVida.ataque, especie);
            defensa = NormativaBioPropiedad.Defecto(PropVida.defensa, especie);
            velocidadDesplazamiento = NormativaBioPropiedad.Defecto(PropVida.velocidadDesplazamiento, especie);
            rangoAtaque = NormativaBioPropiedad.Defecto(PropVida.rangoAtaque, especie);

        }
    }

    void Start()
    {
        AjustarValoresPorDefecto();
        vidaTotal = vida;
    }

    void Update()
    {
        ValidadorDeVida();
    }
    public void RecibirAtaque(float atk) {
        float puntoRealAtk = atk - defensa;
        if (puntoRealAtk < 0 )
        {
            puntoRealAtk = 0;
        }

        vida = vida - puntoRealAtk;
        contadorAtaque++;

        if (contadorAtaque > 3)
        {
            if (defensa > 0)
            {
                defensa = defensa - (atk * 0.1f);
            }
                contadorAtaque = 1;
        }

        barraVida.GetComponent<TextMesh>().text = vida + "/" + vidaTotal;
    }
    public void ValidadorDeVida() {
        if (vida < 0)
        {
            Destroy(gameObject);
        }
    }
    public void VistaVida(bool mostrarVida) {
        barraVida.GetComponent<TextMesh>().text = vida + "/" + vidaTotal;
        barraVida.SetActive(mostrarVida);
    }
}
