﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class NormativaBioPropiedad : MonoBehaviour
{
    float vida;
    int nivel;
    float ataque;
    float defensa;
    float velocidadDesplazamiento;
    float rangoAtaque;

    public static float Defecto(PropVida propiedad, Especie especie) {
        float ret = -1;

        switch (especie)
        {
            case Especie.humano:
                switch (propiedad)
                {
                    case PropVida.vida:
                        ret = 100;
                        break;
                    case PropVida.nivel:
                        ret = 10;
                        break;
                    case PropVida.ataque:
                        ret = 10;
                        break;
                    case PropVida.defensa:
                        ret = 5;
                        break;
                    case PropVida.velocidadDesplazamiento:
                        ret = 1;
                        break;
                    case PropVida.rangoAtaque:
                        ret = 1;
                        break;
                    default:
                        break;
                }
                break;
            case Especie.vaca:
                switch (propiedad)
                {
                    case PropVida.vida:
                        ret = 50;
                        break;
                    case PropVida.nivel:
                        ret = 1;
                        break;
                    case PropVida.ataque:
                        ret = 0;
                        break;
                    case PropVida.defensa:
                        ret = 10;
                        break;
                    case PropVida.velocidadDesplazamiento:
                        ret = 1;
                        break;
                    case PropVida.rangoAtaque:
                        ret = 0;
                        break;
                    case PropVida.velocidadAtaque:
                        ret = 0;
                        break;
                    default:
                        break;
                }
                break;
            case Especie.ogro:
                break;
            case Especie.buho:
                break;
            case Especie.zorro:
                break;
            case Especie.puma:
                break;
            case Especie.araña:
                break;
            case Especie.raton:
                break;
            case Especie.quimera:
                break;
            case Especie.mano:
                break;
            default:
                break;
        }

        return ret;
    }
}
