﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animar : MonoBehaviour
{
    Animator animacion;
    // Start is called before the first frame update
    void Start()
    {
        animacion = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.anyKey)
        {
            animacion.SetBool("moverse", false);
            animacion.SetBool("ataque", false);
        }
    }
    public void Caminar() {
        animacion.SetBool("moverse", true);
    }
    public void Atacar() {
        animacion.SetBool("ataque", true);
    }
}
