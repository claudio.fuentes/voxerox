﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidadorPiso : MonoBehaviour
{
    private bool suelo = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        suelo = true;
    }
    private void OnTriggerExit(Collider other)
    {
        suelo = false;
    }

    public bool Suelo() {
        return suelo;
    }
}
