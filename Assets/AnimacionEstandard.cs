﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionEstandard : MonoBehaviour
{
    Animator animacion;
    int relojInterno = 1200;
    public GameObject mujido;
    void Start()
    {
        animacion = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        relojInterno++;
        if (relojInterno > 1500)
        {
            CambioDeEstado();

            mujido.GetComponent<AudioSource>().Play();
            relojInterno = 0;
        }
    }
    void CambioDeEstado() {
        int seleccion = Random.Range(0, 100);

        if (seleccion >= 0 && seleccion < 20)
        {
            animacion.SetInteger("acciones", 0);
        }
        if (seleccion >= 20 && seleccion < 60)
        {
            animacion.SetInteger("acciones", 1);
        }
        if (seleccion >= 60 && seleccion < 101)
        {
            animacion.SetInteger("acciones", 2);
        }
    }
}
